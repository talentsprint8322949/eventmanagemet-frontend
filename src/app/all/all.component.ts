import { Component } from '@angular/core';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrl: './all.component.css'
})
export class AllComponent {
  all:any;
  constructor(){
  this.all=[{"image":"https://i.pinimg.com/originals/c9/41/fd/c941fd30367d4c660795c5b216bdd11f.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-urban-10.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/ae-gallery-evergreen-13.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/ae-gallery-urban-16.jpg"},
  {"image":"https://i.pinimg.com/originals/18/ce/25/18ce25572ae7d3d8375e55656054e533.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/ae-gallery-urban-20.jpg"},
  {"image":"https://i.pinimg.com/originals/ee/11/08/ee1108fb095b1104e636e1e24b1b9de6.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-ballroom-8.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-ballroom-18.jpg"},
  {"image":"https://image.wedmegood.com/resized/1000X/uploads/project/52795/1558548767_ANAS0929.jpg"},
  {"image":"https://www.dipakstudios.com/gallery/1535304191SAND5712.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-urban-28.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-urban-31.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-ballroom-22.jpg"},
  {"image":"https://www.dipakstudios.com/gallery/1535303839A36I8979-Edit.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/ae-gallery-urban-13.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-urban-13.jpg"},
  {"image":"https://i.pinimg.com/originals/6f/87/f0/6f87f0e92773f6de21f553610760ff34.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-ballroom-50.jpg"},
  {"image":"https://wallpaperaccess.com/full/2340918.jpg"},{"image":"https://adornmentevents.com/wp-content/uploads/ae-gallery-ballroom-22.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/ae-gallery-urban-49.jpg"},
  {"image":"https://i.pinimg.com/originals/05/77/f1/0577f171a3dbb02b2d8e287272b662ad.jpg"},
  {"image":"https://i.pinimg.com/originals/18/ce/25/18ce25572ae7d3d8375e55656054e533.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-urban-48.jpg"},
  {"image":"https://www.maharaniweddings.com/media/gallery/31685-aa-crmn-3096-orig.jpeg"},{"image":"https://adornmentevents.com/wp-content/uploads/ae-gallery-evergreen-3.jpg"},
{"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-ballroom-43.jpg"},
{"image":"https://adornmentevents.com/wp-content/uploads/ae-gallery-urban-35.jpg"},{"image":"https://www.liverpooltownhall.co.uk/wp-content/uploads/2018/01/Small-Ballroom.jpg"}]
  }
}