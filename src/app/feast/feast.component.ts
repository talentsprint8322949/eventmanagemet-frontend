import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerService } from '../customer.service';
import { NgToastService } from 'ng-angular-popup';

@Component({
  selector: 'app-feast',
  templateUrl: './feast.component.html',
  styleUrl: './feast.component.css'
})
export class FeastComponent {

  custId:any;
  cartData:any;
  // Food: any[] = [];
  // category: any;

  // constructor(private service: CustomerService, private route: ActivatedRoute, private router: Router) {}

  // ngOnInit(): void {
  //   this.route.paramMap.subscribe(params => {
  //     this.category = params.get('category');
  //     console.log(this.category)
  //     this.getServiceByCategory(this.category);
  //   });
  // }

  // getServiceByCategory(category: string): void {
  //   this.service.getServiceByCategory(category).subscribe((data: any) => {
  //     this.Food = data;
  //     console.log('Feast:', this.Food);
  //   }, error => {
  //     console.error('Error fetching decorations:', error);
  //   });
  // }


  // back(){
  //   this.router.navigate(['/services'])
  // }

  Food:any;
  Items:any;
  category:any;
  Services: any[] = [];
  // category: any;
constructor(private service:CustomerService,private router:Router,private toast:NgToastService){

}
ngOnInit(): void {
  this.service.getAllItems().subscribe((data:any)=>{
    this.Food = data
    // console.log(data);
    this.custId=localStorage.getItem('customerId')
  })
}
// displayMenu(category: string) {
//   this.router.navigate(['/menu', { category: category }]); 
// }

back(){
  this.router.navigate(['/services'])
}



addToCart(food:any){
  console.log('Adding to cart:', food);

  if (!food || !food.itemDescription || !food.itemPrice || !food.category) {
    console.error('Error: Invalid services object or missing required fields.');
    return;
  }
 
  this.cartData={
    itemImage:food.itemImage,
    itemCategory:food.category,
    itemDescription:food.itemDescription,
    itemPrice:food.itemPrice,

    customer:{
      custId:this.custId,
    }
  }
  console.log('Cart data:', this.cartData); 

  this.service.cartItems(this.cartData, this.custId).subscribe(
    (data: any) => {
      // console.log("Response from cartItems:", data);
      this.toast.success({ detail: "Item added to cart successfully", duration: 5000 });
    })
  
    
  }
}
