import { Component } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { NgToastService } from 'ng-angular-popup';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {
  passwordMatch: boolean = false;
  constructor(private service:CustomerService,private router:Router,private toast:NgToastService){
  }
  register(registerForm: any) {
    if (registerForm.valid) {
      this.service.getRegister(registerForm.value).subscribe((data: any) => {
        console.log("register", data);
        // alert("Registration Sucessful!")
  
        this.toast.success({detail:'Sucess Message',summary:'Registration Sucessful',duration:5000})
        this.router.navigate(['/login']);
      },
        (error: any) => {
          console.log("error", error);
          this.toast.error({detail:'Failure Message',summary:'Registration Failed',duration:5000})
        })
        
  }
  
  }
 



}
