import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admincustomer',
  templateUrl: './admincustomer.component.html',
  styleUrl: './admincustomer.component.css'
})
export class AdmincustomerComponent implements OnInit{
  customerDetails: any[] = [];

  constructor(private customerService: CustomerService) { }

  ngOnInit(): void {
    this.fetchCustomerDetails();
  }

  fetchCustomerDetails(): void {
    this.customerService.getAllCustomerDetails().subscribe((data: any) => {
      this.customerDetails = data; 
    });
  }
  
}
