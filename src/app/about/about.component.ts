import { Component } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrl: './about.component.css'
})
export class AboutComponent {


  images = [
    { id: 1, url: 'https://adornmentevents.com/wp-content/uploads/Yaddira-Quesada.jpg', name: 'Yaddira Quesada' },
    { id: 2, url: 'https://media.licdn.com/dms/image/C5603AQGuzGDCW5LACA/profile-displayphoto-shrink_800_800/0/1594922223918?e=2147483647&v=beta&t=slbtrk6qlHVtt_f6oAea9OdQ32whD2uO6_LnrrMFiCM', name: 'Olga Duran' },
    { id:3, url: 'https://media.licdn.com/dms/image/D5603AQExw9-rw1tcmQ/profile-displayphoto-shrink_800_800/0/1696380739566?e=2147483647&v=beta&t=xIY0-F9vfSyzDaxpicjFXDavOBCCL-yCsyxi3kxoQa0', name: 'Angelynn' },
    { id: 4, url: 'https://cdn-0.liverampup.com/uploads/celebrity/allie-stuckey.jpg', name: 'Carisa Cooper Smith' },
    { id: 5, url: 'https://papiphotos.remax-im.com/Person/100073976/MainPhoto_cropped/MainPhoto_cropped.jpg', name: 'Ellie Howells' },
    { id:6, url: 'https://ignite520.com/wp-content/uploads/2020/08/Allyson-1.jpg', name: 'Molly Stuckey' }
  ];
showName:number | null = null;
  toggleName(imageId: number) {
    this.showName = this.showName === imageId ? null : imageId;
  }

  mouseOver() {
  }

  mouseLeave() {
  }
}
