import { Component } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';
import { NgToastService } from 'ng-angular-popup';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'] 
})
export class LoginComponent {

  customers: any[] = []; 
  emailId: any;
  password: any;
  customerId:any
flag:any;
isAdmin: boolean = false;
  constructor(public service: CustomerService, private router: Router, private toast: NgToastService,public admin:AdminService) {
    this.flag=true;
  }

  ngOnInit(): void {
    this.service.login().subscribe((data: any) => {
      this.customers = data;
      // console.log(data);
    });
  }

  login(loginForm: any) {
   
    

    if(loginForm.emailId=='admin@gmail.com'&& loginForm.password=='password'){
      this.toast.success({detail:"Sucess Message","summary":"Login Sucessful","duration":5000})
      this.admin.setAdminLoggedIn();
      this.router.navigate(['/adminhome'])


      
      this.flag=false;
      // this.isAdmin = true;
    }
    else{
      this.customers.forEach((customer:any)=>{
        if(loginForm.emailId==customer.emailId&&loginForm.password==customer.password){
          console.log(customer)
          localStorage.setItem('customerId',customer.customerId);
          console.log(customer.customerId)
          this.toast.success({detail:"Sucess Message","summary":"Login Sucessful","duration":5000})
          this.service.setUserLoggedIn();
          this.router.navigate(["/home"])
          this.flag=false;
          // this.isAdmin = false;
        }
      })
    }
    if(this.flag){
      this.toast.error({"detail":"Failed Message","summary":"Login Failed","duration":5000})
    }
  }

  logindisable(){
    this.service.setUserLoggedIn();
  }
}


  