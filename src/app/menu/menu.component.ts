import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { ActivatedRoute, Route, Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrl: './menu.component.css'
})
export class MenuComponent implements OnInit {

  // menuItems:any;
  // name:any;
  // category:any
  // constructor(private service:CustomerService,private route:ActivatedRoute,private router:Router){

  // }
  // ngOnInit() {
  //   this.route.paramMap.subscribe(params => {
  //     this.name = params.get('name');
  //     this.service.getMenuByName(this.name).subscribe((data: any) => {
  //       this.menuItems = data;
  //       console.log('Menu Items:', data); 
  //     });
  //   });
  // }
  // // goToFeast(category: string): void {
  // //   this.router.navigate(['/feast', { category: category }]);
  // // }
  // back(){
  //   this.router.navigate(['/services'])
  // }
  

  menuItems:any;
  category:any;
  constructor(private service:CustomerService,private route:ActivatedRoute,private router:Router){

  }
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.category = params.get('category');
      this.service.getMenuByName(this.category).subscribe((data: any) => {
        this.menuItems = data;
        console.log('Menu Items:', data); 
      });
    });
  }
  back(){
    this.router.navigate(['/feast'])
  }
}
