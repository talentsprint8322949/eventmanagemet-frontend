import { Component, OnInit, Renderer2 } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';
import { NgToastService } from 'ng-angular-popup';
declare var jQuery:any;
declare var Razorpay:any;
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent  {
  isPaymentSuccess: boolean = false;
  minEventDate:string='';
  selectedCartItem: any = {};
  bookEvent = {
    phoneNumber: '',
    eventDate: '',
    address: ''
  };
  // isPaymentSuccess: boolean = false;
  cartItems: any[] = [];
  // cartItems: any;
  custId:any;
  cart:any;

 

 
  constructor(private service:CustomerService,private router: Router,private toast:NgToastService,private renderer:Renderer2){}
  ngOnInit(): void {
  this.custId=localStorage.getItem('customerId');
  if(this.custId){
    this.service.getCartItems(this.custId).subscribe((data:any)=>{
      this.cartItems=data
    });
  }else{
    this.toast.warning({detail:"cart Items not added","duration":5000});
  }
    this.minEventDate = this.todayDate();
  // this.getServices();
  this.custId=localStorage.getItem('customerId');
  }


  deleteCartItems(cartId: any) {
    this.service.delCartItems(cartId).subscribe(() => {
      
      this.cartItems = this.cartItems.filter((cart: any) => cart.cartId !== cartId);
      this.toast.success({ detail: "Cart item removed","summary":"Deleted Sucessfully", duration: 5000 });
    }, (error) => {
      console.error("Error deleting cart item:", error);
      this.toast.error({ detail: "Error deleting cart item", duration: 5000 });
    }
  
  );
  }
  

  calculateTotalPrice(): number {
    let total = 0;
    if (Array.isArray(this.cartItems)) {
      for (const item of this.cartItems) {
        total += item.itemPrice; 
      }
    }
    return total;
  }
  
  todayDate(): string {
    const today = new Date();
    const year = today.getFullYear();
    const month = (today.getMonth() + 1).toString().padStart(2, '0');
    const day = today.getDate().toString().padStart(2, '0');
    return `${year}-${month}-${day}`;
  }
  
  



  empModal(): void {
    this.empModal
 
    jQuery('#empModal').modal('show'); 
  }
  isFormValid(): boolean {
    return !!this.bookEvent.phoneNumber && !!this.bookEvent.eventDate && !!this.bookEvent.address;
  }
  

  bookevent(): void {
    const eventData = {
      phoneNumber: this.bookEvent.phoneNumber,
      eventDate: this.bookEvent.eventDate,
      totalPrice: this.calculateTotalPrice()*100,
      address: this.bookEvent.address,
      itemName: this.selectedCartItem.itemName 
    };

    this.service.regcustomerbycustId(eventData,localStorage.getItem('customerId')).subscribe(
      (data: any) => {
        this.toast.success({ detail: 'Event booked successfully', duration: 5000 });
        // this.emptyCart(); 
        this.router.navigate(['/booked']);
        this.deleteAllCartItems();
      },
      (error: any) => {
        console.error('Error booking event:', error);
        this.toast.error({ detail: 'Error booking event', duration: 5000 });
      }
    );
  }
  deleteAllCartItems() {
    if (this.custId) {
      this.service.delAllCartItems(this.custId).subscribe((data:any) => {
        this.cartItems = [];
        this.toast.success({ detail: "Cart items deleted", summary: "Deleted Successfully", duration: 5000 });
      });
    }
  }
  // emptyCart() {
  //   this.cartItems = []; 
  // }
  payNow(){
    // this.emptyCart();
 const RazorpayOptions={
  description:'Sample Razorpay demo',
  currency:'INR',
  amount:this.calculateTotalPrice()*100,
  name:"Navya",
  key:"rzp_test_7UuEvVigA1V8Bz",
  theme:{
    color:'#0c238a'
  },
  modal: {
    ondismiss:()=>{
this.toast.warning({detail:"Failed",summary:"Payment cancelled",duration:5000})
    }
  }
 }
 const successCallback=(paymentid:any)=>{
  this.toast.success({detail:"Sucess",summary:"Payment Sucess",duration:5000});
  // this.isPaymentSuccess = true; 
      // this.bookevent();
//  this.bookingsubmit();


}

const errorCallback=(error:any)=>{
  
  this.toast.error({detail:"Failed",summary:"Payment Failed",duration:5000});
}

Razorpay.open(RazorpayOptions,successCallback,errorCallback)
  }

  addMore(){
    this.router.navigate(['/services'])
  }

  startAnimation() {
    const amount = 60; // Increase count to 60 particles
    const type = 'shadow'; // Particle type

    for (let i = 0; i < amount; i++) {
      const x = Math.random() * window.innerWidth; // Random X position within window width
      const y = Math.random() * window.innerHeight; // Random Y position within window height
      this.createParticle(x, y, type);
    }
  }

  createParticle(x: number, y: number, type: string) {
    const particle = this.renderer.createElement('particle');
    this.renderer.appendChild(document.body, particle);

    let width = Math.random() * 5 + 4;
    let height = width;
    const color = `hsl(${Math.random() * 90 + 90}, 70%, 50%)`; // Quotes around color value

    this.renderer.setStyle(particle, 'boxShadow', `0 0 ${Math.floor(Math.random() * 10 + 10)}px ${color}`);
    this.renderer.setStyle(particle, 'background', color);
    this.renderer.setStyle(particle, 'borderRadius', '50%');
    this.renderer.setStyle(particle, 'width', `${width}px`); // Quotes around width value
    this.renderer.setStyle(particle, 'height', `${height}px`); // Quotes around height value
    this.renderer.setStyle(particle, 'position', 'absolute');
    this.renderer.setStyle(particle, 'left', `${x}px`); // Quotes around x value
    this.renderer.setStyle(particle, 'top', `${y}px`); // Quotes around y value

    this.animateParticle(particle, x, y);
  }

  animateParticle(particle: HTMLElement, x: number, y: number) {
    const destinationX = (Math.random() - 0.5) * 300;
    const destinationY = (Math.random() - 0.5) * 300;
    const rotation = Math.random() * 520;
    const delay = Math.random() * 200;

    const animation = particle.animate(
      [
        { transform: `translate(-50%, -50%) translate(${x}px, ${y}px) rotate(0deg)`, opacity: 1 }, // Quotes around translate values
        {
          transform: `translate(-50%, -50%) translate(${x + destinationX}px, ${y + destinationY}px) rotate(${rotation}deg)`,
          opacity: 0,
        },
      ],
      {
        duration: Math.random() * 10000 + 10000, // Increase duration to a range between 10s to 12s
        easing: 'cubic-bezier(0, .9, .57, 1)',
        delay: delay,
      }
    );

    animation.onfinish = () => {
      particle.remove();
    };
  }

  
}
