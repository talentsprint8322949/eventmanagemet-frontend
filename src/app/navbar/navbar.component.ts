import { Component } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.css'
})
export class NavbarComponent {
  constructor(private router: Router,public service:CustomerService,public admin:AdminService) {

  }

  navigateToCategory(category: string) {
    switch (category) {
      case 'Decor':
        this.router.navigate(['/adminmain/decor']);
        break;
      case 'makeup':
        this.router.navigate(['/adminmain/makeup']);
        break;
      case 'dinning':
        this.router.navigate(['/adminmain/dinning']);
        break;
      default:
        
        break;
    }
  }

  // getCartItemCount(): number {
  //   return this.cartItems.length;
  // }
}
