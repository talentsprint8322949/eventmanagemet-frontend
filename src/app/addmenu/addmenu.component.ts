import { Component } from '@angular/core';
import { AdminService } from '../admin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addmenu',
  templateUrl: './addmenu.component.html',
  styleUrl: './addmenu.component.css'
})
export class AddmenuComponent {

  newMenu: any = {};
constructor(private service:AdminService,private router:Router){}
addMenu(){
if(this.newMenu.menuId && this.newMenu.itemName && this.newMenu.category){
  this.service.registerMenu(this.newMenu).subscribe((data:any)=>{
    console.log("Menu Items added sucessfully: ", data);
    this.newMenu={};
  },
  (error)=>{
    console.error('Error adding menu items', error)
  }
)
}
}
// back(){
//   this.router.navigate(['/adminmenu']);
// }
}
