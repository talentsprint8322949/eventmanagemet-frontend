// import { Component } from '@angular/core';
// import { CustomerService } from '../customer.service';
// import { ActivatedRoute, Router } from '@angular/router';

// @Component({
//   selector: 'app-makeup',
//   templateUrl: './makeup.component.html',
//   styleUrl: './makeup.component.css'
// })
// export class MakeupComponent {

//   packages: any[] = [];
//   category: any;

//   constructor(private service: CustomerService, private route: ActivatedRoute, private router: Router) {}

//   ngOnInit(): void {
//     this.route.paramMap.subscribe(params => {
//       this.category = params.get('category');
//       console.log(this.category)
//       this.getServiceByCategory(this.category);
//     });
//   }

//   getServiceByCategory(category: string): void {
//     this.service.getServiceByCategory(category).subscribe((data: any) => {
//       this.packages = data;
//       console.log('Makeup:', this.packages);
//     }, error => {
//       console.error('Error fetching decorations:', error);
//     });
//   }


//   back(){
//     this.router.navigate(['/services'])
//   }
// }
