import { Component } from '@angular/core';

@Component({
  selector: 'app-ballroom',
  templateUrl: './ballroom.component.html',
  styleUrl: './ballroom.component.css'
})
export class BallroomComponent {
  ballroom:any;
  constructor(){
  this.ballroom=[{"image":"https://adornmentevents.com/wp-content/uploads/ae-gallery-ballroom-22.jpg"},
  {"image":"https://i.pinimg.com/originals/18/ce/25/18ce25572ae7d3d8375e55656054e533.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-ballroom-22.jpg"},
  {"image":"https://smp-is.stylemepretty.com/submissions/uploads/171382/56ccf530e661a$!900x467.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-ballroom-35.jpg"},
  {"image":"https://i.pinimg.com/originals/bd/50/5e/bd505e16c24afc4375485c10e8bcd237.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-ballroom-51.jpg"},
  {"image":"https://s-media-cache-ak0.pinimg.com/originals/80/a5/89/80a589c3d2d83e3c7827e9353c618a72.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-urban-28.jpg"},{"image":"https://www.maharaniweddings.com/media/gallery/31685-aa-crmn-3096-orig.jpeg"},
  {"image":"https://i.pinimg.com/originals/b9/3a/b1/b93ab14ecc7741bfabffd3efe5287b80.jpg"},{"image":"https://www.liverpooltownhall.co.uk/wp-content/uploads/2018/01/Small-Ballroom.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/ae-gallery-modern-16.jpg"},
  {"image":"https://apis.xogrp.com/media-api/images/30f341d8-b9fa-a0e9-7c6a-a943e99f4365"},{"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-ballroom-15.jpg"} ]
  }
}
