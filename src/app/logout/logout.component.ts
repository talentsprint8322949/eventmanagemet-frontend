import { Component } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';
import { AdminService } from '../admin.service';
import { NgToastService } from 'ng-angular-popup';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrl: './logout.component.css'
})
export class LogoutComponent {

  constructor(private service:CustomerService,private router:Router,private admin:AdminService,private toast:NgToastService){

  }
  ngOnInit(): void {
    // alert("Logout sucessful")
    this.toast.success({detail:"Sucess",summary:"Logout Sucessful",duration:5000})
    this.service.setUserLoggedOut();
    this.admin.setAdminLogout();
    this.router.navigate(['/home']);
  }
}
