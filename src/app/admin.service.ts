import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  isAdmin:boolean;

  constructor(private httpClient:HttpClient) {
    this.isAdmin=false;
   }
   setAdminLoggedIn(){
    this.isAdmin=true;
   }
   setAdminLogout(){
    this.isAdmin=false;
   }
  
   getServiceByCategory(category:any){
    return this.httpClient.get("http://localhost:8085/getServicesByCategory/"+category);
  }

  getMenuByName(category:any){
    return this.httpClient.get("http://localhost:8085/getMenuByName/"+category);
  }

  getAllItems(){
    return this.httpClient.get('http://localhost:8085/getAllItems')
   }

   deleteServices(id:any){
    return this.httpClient.delete("http://localhost:8085/deleteServicesById/"+id)
   }
   deleteItems(id:any){
    return this.httpClient.delete('http://localhost:8085/deleteItemById/'+id)
  }

  registerMenu(menu:any){
    return this.httpClient.post("http://localhost:8085/registerMenu",menu)
       }

       registerService(services:any){
        return this.httpClient.post("http://localhost:8085/registerServices",services);
       }

       updateServices(services:any): any{
        return this.httpClient.put("http://localhost:8085/updateService",services)
      }

      updateItems(menu:any):any{
        return this.httpClient.put("http://localhost:8085/updateItem",menu)
      }
      getAllCustomerDetails(){
        return this.httpClient.get('http://http://localhost:8085/getAllCustomerDetails')
      }


      getEmail(formData:any){
        return this.httpClient.post('http://localhost:8085/api/contact-form',formData)
      }
      
}
