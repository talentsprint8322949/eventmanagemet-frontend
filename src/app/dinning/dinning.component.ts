// import { Component } from '@angular/core';
// import { CustomerService } from '../customer.service';
// import { ActivatedRoute, Router } from '@angular/router';

// @Component({
//   selector: 'app-dinning',
//   templateUrl: './dinning.component.html',
//   styleUrl: './dinning.component.css'
// })
// export class DinningComponent {

//   Dinnings: any[] = [];
//   category: any;

//   constructor(private service: CustomerService, private route: ActivatedRoute, private router: Router) {}

//   ngOnInit(): void {
//     this.route.paramMap.subscribe(params => {
//       this.category = params.get('category');
//       console.log(this.category)
//       this.getServiceByCategory(this.category);
//     });
//   }

//   getServiceByCategory(category: string): void {
//     this.service.getServiceByCategory(category).subscribe((data: any) => {
//       this.Dinnings = data;
//       console.log('Dinnings:', this.Dinnings);
//     }, error => {
//       console.error('Error fetching decorations:', error);
//     });
//   }

//   back(): void {
//     this.router.navigate(['/services']);
//   }
// }
