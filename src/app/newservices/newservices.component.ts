import { Component } from '@angular/core';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-newservices',
  templateUrl: './newservices.component.html',
  styleUrl: './newservices.component.css'
})
export class NewservicesComponent {
  newProduct: any = {
    name: '',
    description: '',
    price: null,
    category: '',
    image: ''
  };
  selectedCategory: string = '';
  showImageInput: boolean = false;

  constructor(private service: AdminService) {}

  addNewProduct() {
    switch (this.selectedCategory) {
      case 'Decor':
        this.service.registerService(this.newProduct).subscribe(
          (response) => {
            console.log('decor product added successfully', response);
            this.clearForm();
          },
          (error) => {
            console.error('Error adding Card product:', error);
          }
        );
        break;
      case 'makeup':
        this.service.registerService(this.newProduct).subscribe(
          (response) => {
            console.log('Makeup product added successfully', response);
            this.clearForm();
          },
          (error) => {
            console.error('Error adding makeup product:', error);
          }
        );
        break;
        case 'dinning':
          this.service.registerService(this.newProduct).subscribe(
            (response) => {
              console.log('dinning product added successfully', response);
              this.clearForm();
            },
            (error) => {
              console.error('Error adding  product:', error);
            }
          );
          break;
      default:
        console.error('Invalid category:', this.selectedCategory);
    }
  }

  selectCategory() {
    this.showImageInput = this.selectedCategory !== '';
  }

  clearForm() {
    this.newProduct = {
      name: '',
      description: '',
      price: null,
      category: '',
      image: ''
    };
    this.selectedCategory = '';
    this.showImageInput = false;
  }

  
}
