import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  isUserLogged:boolean
  isLoggedIn:boolean=false;
 
  constructor(private httpClient : HttpClient) {
    this.isUserLogged=false
  }
  setUserLoggedIn(){
    this.isUserLogged=true
 }
   setUserLoggedOut(){
    this.isUserLogged=false
   }
   getStatus(){
    return this.isUserLogged
   }

   getRegister(customer:any){
    return this.httpClient.post('http://localhost:8085/getRegister',customer);
  }
  login(){
   return this.httpClient.get("http://localhost:8085/getAllCustomers")
  }

  getServiceByCategory(category:any){
    return this.httpClient.get("http://localhost:8085/getServicesByCategory/"+category);
  }

  getMenuByName(category:any){
    return this.httpClient.get("http://localhost:8085/getMenuByName/"+category);
  }

  getServicesByName(name:any){
    return this.httpClient.get("http://localhost:8085/getServicesByName/"+name);
  }

  getAllItems(){
    return this.httpClient.get('http://localhost:8085/getAllItems')
   }

   addToCart(services: any){
    return this.httpClient.post('http://localhost:8085/registerCart',services)
   }

  //  getCartItems(){
  //   return this.httpClient.get('http://localhost:8085/getAllProducts')
  //  }

   removeFromCart(id:any){
      return this.httpClient.delete("http://localhost:8085/deleteByItemId/"+id)
   }

   bookEvent(customerdetails:any){
    return this.httpClient.post("http://localhost:8085/bookEvent",customerdetails)
   }

   getAllCustomerDetails(){
    return this.httpClient.get("http://localhost:8085/getAllCustomerDetails")
   }

   loginUser(login:any){
      return this.httpClient.post("http://localhost:8085/registerUser",login)
   }
   addToCartFood(food:any){
    return this.httpClient.post("http://localhost:8085/registerItems",food)
   }

   sendEmail(formData: any) {
    return this.httpClient.post<any>('http://localhost:8085/sendEmail', formData);
  }

  cartItems(cartData: any, custId: string) {
    console.log('Sending cart data:', cartData);
    return this.httpClient.post(`http://localhost:8085/regCartbyCustId/${custId}`, cartData,{responseType:'text'});
  }
  
  getCartItems(custId: string) {
    return this.httpClient.get(`http://localhost:8085/getProductsById/${custId}`);
  }
  
  delCartItems(cartId: any) {
    return this.httpClient.delete(`http://localhost:8085/deleteByItemId/${cartId}`, { responseType: 'text' });
  }

  
  regcustomerbycustId(eventData:any,custId:any){
    return this.httpClient.post(`http://localhost:8085/bookEvent/${custId}`,eventData,{responseType:'text'})
  }

  getcustBycustId(custId:any){
    return this.httpClient.get(`http://localhost:8085/getCustomerDetailsById/${custId}`)
  }

  delAllCartItems(custId:any){
    return this.httpClient.delete(`http://localhost:8085/deleteCartBycustId/${custId}`);
  }
}
