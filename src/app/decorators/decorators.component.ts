import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from '../admin.service';
import { NgToastService } from 'ng-angular-popup';

@Component({
  selector: 'app-decorators',
  templateUrl: './decorators.component.html',
  styleUrls: ['./decorators.component.css']
})
export class DecoratorsComponent implements OnInit {
  Services: any[] = [];
  category: any;
  custId:any;
  cartData:any;

  constructor(private service: CustomerService, private route: ActivatedRoute, private router: Router,private toast:NgToastService) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.category = params.get('category');
      console.log(this.category)
      this.getServiceByCategory(this.category);
      this.custId=localStorage.getItem('customerId')
    });

  }

  getServiceByCategory(category: any): void {
    this.service.getServiceByCategory(category).subscribe((data: any) => {
      this.Services = data;
      console.log('Services:', this.Services);
    }, error => {
      console.error('Error fetching decorations:', error);
    });
  }

  back(): void {
    this.router.navigate(['/services']);
  }



// addToCart(services: any) {
//   if (!services || !services.name || !services.description || !services.price || !services.category) {
//     console.error('Error: Invalid services object or missing required fields.');
//     return; 
//   }

//   console.log('Adding to cart:', services); 

//   this.service.addToCart(services).subscribe(
//     (data: any) => {
//       console.log("Service added to the cart", data);
      
//     },
//     (error: any) => {
//       console.error("Error adding service to cart:", error);
//     }
//   );
// }


addToCart(services:any){
  console.log('Adding to cart:', services);

  if (!services || !services.name || !services.description || !services.price || !services.category) {
    console.error('Error: Invalid services object or missing required fields.');
    return;
  }
 
  this.cartData={
    itemName:services.name,
    itemImage:services.image,
    itemCategory:services.category,
    itemDescription:services.description,
    itemPrice:services.price,

    customer:{
      custId:this.custId,
    }
  }
  console.log('Cart data:', this.cartData); 

  this.service.cartItems(this.cartData, this.custId).subscribe(
    (data: any) => {
      // console.log("Response from cartItems:", data);
      this.toast.success({ detail: "Item added to cart successfully", duration: 5000 });
    })
  
    
  }
}
