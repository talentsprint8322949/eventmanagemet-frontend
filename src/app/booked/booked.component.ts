import { Component, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-booked',
  templateUrl: './booked.component.html',
  styleUrls: ['./booked.component.css']
})
export class BookedComponent {

  constructor(private renderer: Renderer2) {}

  ngOnInit(): void {}

  // startAnimation() {
  //   const amount = 60; // Increase count to 60 particles
  //   const type = 'shadow'; // Particle type

  //   for (let i = 0; i < amount; i++) {
  //     const x = Math.random() * window.innerWidth; // Random X position within window width
  //     const y = Math.random() * window.innerHeight; // Random Y position within window height
  //     this.createParticle(x, y, type);
  //   }
  // }

  // createParticle(x: number, y: number, type: string) {
  //   const particle = this.renderer.createElement('particle');
  //   this.renderer.appendChild(document.body, particle);

  //   let width = Math.random() * 5 + 4;
  //   let height = width;
  //   const color = `hsl(${Math.random() * 90 + 90}, 70%, 50%)`; // Quotes around color value

  //   this.renderer.setStyle(particle, 'boxShadow', `0 0 ${Math.floor(Math.random() * 10 + 10)}px ${color}`);
  //   this.renderer.setStyle(particle, 'background', color);
  //   this.renderer.setStyle(particle, 'borderRadius', '50%');
  //   this.renderer.setStyle(particle, 'width', `${width}px`); // Quotes around width value
  //   this.renderer.setStyle(particle, 'height', `${height}px`); // Quotes around height value
  //   this.renderer.setStyle(particle, 'position', 'absolute');
  //   this.renderer.setStyle(particle, 'left', `${x}px`); // Quotes around x value
  //   this.renderer.setStyle(particle, 'top', `${y}px`); // Quotes around y value

  //   this.animateParticle(particle, x, y);
  // }

  // animateParticle(particle: HTMLElement, x: number, y: number) {
  //   const destinationX = (Math.random() - 0.5) * 300;
  //   const destinationY = (Math.random() - 0.5) * 300;
  //   const rotation = Math.random() * 520;
  //   const delay = Math.random() * 200;

  //   const animation = particle.animate(
  //     [
  //       { transform: `translate(-50%, -50%) translate(${x}px, ${y}px) rotate(0deg)`, opacity: 1 }, // Quotes around translate values
  //       {
  //         transform: `translate(-50%, -50%) translate(${x + destinationX}px, ${y + destinationY}px) rotate(${rotation}deg)`,
  //         opacity: 0,
  //       },
  //     ],
  //     {
  //       duration: Math.random() * 2000 + 10000, // Increase duration to a range between 10s to 12s
  //       easing: 'cubic-bezier(0, .9, .57, 1)',
  //       delay: delay,
  //     }
  //   );

  //   animation.onfinish = () => {
  //     particle.remove();
  //   };
  // }

}
