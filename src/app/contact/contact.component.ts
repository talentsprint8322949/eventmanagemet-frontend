import { Component } from '@angular/core';
import { NgToastService } from 'ng-angular-popup';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent {
  formData: any = {
  name:'',
  email:'',
  contactNumber:'',
  location:'',
  guestCount:'',
  moreDetails:''
  };




  constructor(private toast:NgToastService,private service:AdminService) {}

  onSubmit(contactForm:any): void {
    this.formData.name=contactForm.name;
    this.formData.email=contactForm.email;
    this.formData.contactNumber=contactForm.contactNumber;
    this.formData.location=contactForm.location;
    this.formData.guestcount=contactForm.guestCount;
    this.formData.moreDetails=contactForm.moreDetails
    // console.log('Form submitted:', this.formData);
    this.service.getEmail(this.formData).subscribe((data:any)=>{
      if(typeof data=='string'){
        this.toast.success({detail:"Sucess",summary:"Sucessfully Submitted the Form",duration:5000})
      }else{
        this.toast.error({detail:"Failed",summary:"Subimission Failed",duration:5000})

      }
    })
   
  
    this.clearForm();
  }

  clearForm(): void {
    this.formData = {}; 
  }
}
