import { Component } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrl: './services.component.css'
})
export class ServicesComponent {

  constructor(private router:Router,private service:CustomerService){
    // this.service.setUserLoggedIn();
  }
  
  navigateToDecorators(category: string): void {
    this.router.navigate(['/decorators/:catgeory', { category: category }]);
  }
  navigateToFood(category: string): void {
    this.router.navigate(['feast', { category: category }]);
  }
  navigateToMakeup(category: string): void {
    this.router.navigate(['/decorators/:category', { category: category }]);
  }

  navigateToDinnings(category: string): void {
    this.router.navigate(['/decorators/:category', { category: category }]);
  }
}

