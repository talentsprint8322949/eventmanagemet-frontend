import { Component } from '@angular/core';
import { AdminService } from '../admin.service';
import { ActivatedRoute, Router } from '@angular/router';

declare var jQuery:any;
@Component({
  selector: 'app-adminmain',
  templateUrl: './adminmain.component.html',
  styleUrl: './adminmain.component.css'
})
export class AdminmainComponent {
Services: any={};
  category: any;
  constructor(private service: AdminService, private route: ActivatedRoute, private router: Router) {}
  
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.category = params.get('category');
      console.log(this.category)
      this.getServiceByCategory(this.category);
    });
  }

  getServiceByCategory(category: any): void {
    this.service.getServiceByCategory(category).subscribe((data: any) => {
      this.Services = data;
      console.log('Services:', this.Services);
    }, error => {
      console.error('Error fetching decorations:', error);
    });
  }

  back(): void {
    this.router.navigate(['/adminhome']);
  }

  deleteServices(services:any){
    this.service.deleteServices(services.id).subscribe((data:any)=>{
      // console.log(this.services.id);

    })
    const index = this.Services.findIndex((data:any)=>{
      return data.id==services.id;
    })
    this.Services.splice(index,1)
    alert("Deleted")
  }

  addnew(){
    this.router.navigate(["/newservices"])
  }

  editServices(services:any){
this.Services=services;
jQuery('#empModal').modal('show');
  }

  updateServices(){
    this.service.updateServices(this.Services).subscribe((data:any)=>{
      console.log(data);
    })
  }
}
