import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ServicesComponent } from './services/services.component';
import { ContactComponent } from './contact/contact.component';
import { AllComponent } from './all/all.component';
import { UrbanComponent } from './urban/urban.component';
import { BallroomComponent } from './ballroom/ballroom.component';
import { PartnersComponent } from './partners/partners.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgToastModule } from 'ng-angular-popup';
import { FooterComponent } from './footer/footer.component';
import { AdminComponent } from './admin/admin.component';
import { DecoratorsComponent } from './decorators/decorators.component';
import { FeastComponent } from './feast/feast.component';
// import { DinningComponent } from './dinning/dinning.component';
// import { MakeupComponent } from './makeup/makeup.component';
import { MenuComponent } from './menu/menu.component';
import { LogoutComponent } from './logout/logout.component';
import { AdminhomeComponent } from './adminhome/adminhome.component';
import { AdminmainComponent } from './adminmain/adminmain.component';
import { AdminfeastComponent } from './adminfeast/adminfeast.component';
import { AdminmenuComponent } from './adminmenu/adminmenu.component';
import { AddmenuComponent } from './addmenu/addmenu.component';
import { NewservicesComponent } from './newservices/newservices.component';
import { CartComponent } from './cart/cart.component';

import { AdmincustomerComponent } from './admincustomer/admincustomer.component';
import { BookedComponent } from './booked/booked.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    ServicesComponent,
    ContactComponent,
    AllComponent,
    UrbanComponent,
    BallroomComponent,
    PartnersComponent,
    AboutComponent,
    HomeComponent,
    FooterComponent,
    AdminComponent,
    DecoratorsComponent,
    FeastComponent,
    // DinningComponent,
    // MakeupComponent,
    MenuComponent,
    LogoutComponent,
    AdminhomeComponent,
    AdminmainComponent,
    AdminfeastComponent,
    AdminmenuComponent,
    AddmenuComponent,
    NewservicesComponent,
    CartComponent,
    AdmincustomerComponent,
    BookedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    NgToastModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
