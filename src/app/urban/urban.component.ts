import { Component } from '@angular/core';

@Component({
  selector: 'app-urban',
  templateUrl: './urban.component.html',
  styleUrl: './urban.component.css'
})
export class UrbanComponent {
  urban:any;
  constructor(){
  this.urban=[{"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-urban-48.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-urban-10.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/ae-gallery-evergreen-9.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-urban-31.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/ae-gallery-urban-20.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/ae-gallery-evergreen-13.jpg"},{"image":"https://adornmentevents.com/wp-content/uploads/ae-gallery-evergreen-3.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-urban-4.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/ae-gallery-urban-35.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-ballroom-18.jpg"},{"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-urban-43.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/ae-gallery-urban-49.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/ae-gallery-urban-16.jpg"},
  {"image":"https://adornmentevents.com/wp-content/uploads/gallery2023-urban-28.jpg"},{"image":"https://adornmentevents.com/wp-content/uploads/ae-gallery-urban-42.jpg"}]
  }
}
