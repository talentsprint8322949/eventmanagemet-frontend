import { Component } from '@angular/core';
import { AdminService } from '../admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgToastService } from 'ng-angular-popup';

declare var jQuery:any;
@Component({
  selector: 'app-adminmenu',
  templateUrl: './adminmenu.component.html',
  styleUrl: './adminmenu.component.css'
})
export class AdminmenuComponent {
  menuItems:any;
  category:any;
  constructor(private service:AdminService,private route:ActivatedRoute,private router:Router,private toast:NgToastService){

  }
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.category = params.get('category');
      this.service.getMenuByName(this.category).subscribe((data: any) => {
        this.menuItems = data;
        console.log('Menu Items:', data); 
      });
    });
  }
  back(){
    this.router.navigate(['/adminfeast'])
  }
delete(menu:any){
  this.service.deleteItems(menu.menuId).subscribe((data:any)=>{

  })
  const index = this.menuItems.findIndex((data:any)=>{
return data.menuId==menu.menuId
  })
  this.menuItems.splice(index,1)
  this.toast.success({"detail":"Sucess","summary":"Deleted Sucessful","duration":5000})
}


addNew(){
  this.router.navigate(["/addmenu"])
}

updateMenu(menu:any){
this.menuItems=menu;
jQuery('#empModal').modal('show');
}

updateItems(){
  this.service.updateItems(this.menuItems).subscribe((data:any)=>{
    console.log(data);
  })
}


// editServices(services:any){
//   this.Services=services;
//   jQuery('#empModal').modal('show');
//     }
  
//     updateServices(){
//       this.service.updateServices(this.Services).subscribe((data:any)=>{
//         console.log(data);
//       })
//     }
}
