import { Component } from '@angular/core';
import { AdminService } from '../admin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adminfeast',
  templateUrl: './adminfeast.component.html',
  styleUrl: './adminfeast.component.css'
})
export class AdminfeastComponent {
  Food:any;
  Items:any;
  category:any;
constructor(private service:AdminService,private router:Router){

}
  ngOnInit(): void {
    this.service.getAllItems().subscribe((data:any)=>{
      this.Food = data
      console.log(data);
    })
  }
  // displayMenu(category: string) {
  //   this.router.navigate(['/adminmenu', { category: category }]); 
  // }
  
  back(){
    this.router.navigate(['/adminhome'])
  }
  
}
