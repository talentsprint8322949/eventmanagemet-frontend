import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { AllComponent } from './all/all.component';
import { UrbanComponent } from './urban/urban.component';
import { BallroomComponent } from './ballroom/ballroom.component';
import { HomeComponent } from './home/home.component';
import { ServicesComponent } from './services/services.component';
import { PartnersComponent } from './partners/partners.component';
import { DecoratorsComponent } from './decorators/decorators.component';
import { FeastComponent } from './feast/feast.component';
// import { MakeupComponent } from './makeup/makeup.component';
// import { DinningComponent } from './dinning/dinning.component';
import { MenuComponent } from './menu/menu.component';
import { LogoutComponent } from './logout/logout.component';
import { AdminhomeComponent } from './adminhome/adminhome.component';
import { AdminmainComponent } from './adminmain/adminmain.component';
import { AdminfeastComponent } from './adminfeast/adminfeast.component';
import { AdminmenuComponent } from './adminmenu/adminmenu.component';
import { AddmenuComponent } from './addmenu/addmenu.component';
import { NewservicesComponent } from './newservices/newservices.component';
import { CartComponent } from './cart/cart.component';

import { AdmincustomerComponent } from './admincustomer/admincustomer.component';
import { BookedComponent } from './booked/booked.component';

const routes: Routes = [
  {path:"login",component:LoginComponent},
  {path:"register",component:RegisterComponent},
  {path:"contact",component:ContactComponent},
  {path:"about",component:AboutComponent},
  {path:"all",component:AllComponent},
  {path:"urban",component:UrbanComponent},
  {path:"ballroom",component:BallroomComponent},
  {path:"home",component:HomeComponent},
  {path:"services",component:ServicesComponent},
  {path:"partners",component:PartnersComponent},
  {path:"decorators/:categoty",component:DecoratorsComponent},
  { path: 'feast', component: FeastComponent },
  {path:"menu",component:MenuComponent},
  {path:"logout",component:LogoutComponent},
  {path:"adminhome",component:AdminhomeComponent},
  {path:"adminmain/:category",component:AdminmainComponent},
  {path:"adminfeast",component:AdminfeastComponent},
  {path:"adminmenu",component:AdminmenuComponent},
  {path:"addmenu",component:AddmenuComponent},
  {path:"newservices",component:NewservicesComponent},
  {path:"addtocart",component:CartComponent},
 
  {path:"customerdetails",component:AdmincustomerComponent},
  {path:"booked",component:BookedComponent}
 

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
